import { ChangeEvent, FunctionComponent, ReactElement, useState } from "react";
import "./App.css";

const App: FunctionComponent = (): ReactElement => {
  const [name, setName] = useState<string>();

  return (
    <div className="app">
      <h1>React App</h1>
      <div>
        <label htmlFor="name">
          Name:
          <input
            id="name"
            type="text"
            value={name}
            onChange={(event: ChangeEvent<HTMLInputElement>): void =>
              setName(event?.target?.value)
            }
          />
        </label>
      </div>
    </div>
  );
};

export default App;
