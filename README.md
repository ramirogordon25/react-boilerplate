<img src="./src/logo.png" alt="react boilerplate banner" align="center" />

<br />

# React Boilerplate

<div align="center"><strong>Start your next react project in seconds</strong></div>
<div align="center">A highly scalable, offline-first foundation with the best DX and a focus on performance and best practices</div>

## Features build-in

1. ReactJS with TypeScript
2. Babel v8
3. Webpack v5
   - React Fast Refresh
   - Source Maps
   - Code splitting and optimization
4. ESLint
5. Prettier

## Getting Started

1. Clone this repo using `git clone --depth=1 https://gitlab.com/ramirogordon25/react-boilerplate.git <YOUR_PROJECT_NAME>`.<br />
2. Move to the appropriate directory: `cd <YOUR_PROJECT_NAME>`.<br />
3. Run `npm install` in order to install dependencies and clean the git repo.<br />
4. Run `npm run start` to see the example app at `http://localhost:3000`.

## Prerequisites

Requirements for the software and other tools to build, test and push

- Make sure that you have Node.js v8.15.1 and npm v16 or above installed.

<!-- ## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code
of conduct, and the process for submitting pull requests to us. -->

## Authors

- **Ramiro Gordon**

See also the list of
[contributors](https://gitlab.com/ramirogordon25/react-boilerplate/-/project_members)
who participated in this project.
